# An npm module that generates a slack-compatible emoji representation of english words.

![emojiwriter](example.gif)

# Usage

## With the Party Parrot (:partyparrot:)

Just pass the script a string of your choosing.

```javascript
var emojiWriter = require('emojiwriter');
var convertedString = emojiWriter('amazing');
console.log(convertedString);
```

## Custom Emoji List

Pass an argument to use as your emoji

```javascript
var emojiWriter = require('emojiwriter');
var convertedString = emojiWriter('amazing', ':rage:');
console.log(convertedString);
convertedString = emojiwriter('amazing', [':rage1:', ':rage2:']);
console.log(convertedString);
```

## Custom Whitespace

Pass an argument to use as custom whitespace

```javascript
var emojiWriter = require('emojiwriter');
var convertedString = emojiWriter('amazing', null, ':rage:');
console.log(convertedString);
```

## Invert Whitespace
Pass an argument to invert your emoji and whitespace

```javascript
var emojiWriter = require('emojiwriter');
var convertedString = emojiWriter('amazing', null, null, true);
console.log(convertedString);
```

Inspired/forked/ported from https://github.com/Shrugs/partyparrot by Matt Condon, 2015
