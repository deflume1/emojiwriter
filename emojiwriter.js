var ALPHABET = require('./alphabet');

var PARTY_PARROTS = [':partyparrot:', ':partyparrot2:', ':partyparrot3:', ':partyparrot4:'];

function characterToEmoji(character, emoji, whitespace) {
    if(character === ' ') {
        return '\n\n\n\n\n\n\n\n';
    }
    var outputString = '';
    var arrayEntry = ALPHABET[character];
    for(var row of arrayEntry) {
        for(var col of row) {
            if(col) {
                outputString += emoji;
            } else {
                outputString += whitespace;
            }
        }
        outputString += '\n';
    }
    return outputString;
}

function getRandomInt(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}

function stringToEmoji(string, defaultEmoji, defaultWhitespace, invert) {
    string = string.toLowerCase();
    var outputString = '';
    var useCustomEmoji = !!defaultEmoji;
    var useCustomWhitespace = !!defaultWhitespace;
    for(var character of string) {
        var emoji;
        if(useCustomEmoji) {
            if(!Array.isArray(defaultEmoji)) {
                defaultEmoji = [defaultEmoji];
            }
            emoji = defaultEmoji[getRandomInt(0,defaultEmoji.length-1)];
        } else {
            emoji = PARTY_PARROTS[getRandomInt(0,3)];
        }
        var whitespace = useCustomWhitespace ? defaultWhitespace : '        ';
        if(!invert) {
            outputString += characterToEmoji(character, emoji, whitespace);
        } else {
            outputString += characterToEmoji(character, whitespace, emoji);
        }
        outputString += '\n\n';
    }
    return outputString;
}

module.exports = stringToEmoji;
